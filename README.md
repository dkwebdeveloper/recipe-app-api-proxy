# recipe-app-api-proxy

NGINX proxy app for our receipe APP

# usage

### Environment variables

* 'LISTEN_PORT' - Port to listen on (default: '8080')
* 'APP_HOST' - Hostname of the app to forward request to (default: 'app')
* 'ALL_PORT' - Port of the app to forward request to (default: '9000')
